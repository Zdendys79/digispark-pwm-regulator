// Digispark Attini85 kickstarter - as computer fan regulator
  // board picture: https://www.openimpulse.com/blog/wp-content/uploads/2015/11/Digispark-ATTINY85-Development-Board-1.jpg
  // circuit schema: https://crcit.net/c/f71e125f9115474baeb99b27f20d0082 
  // calculations: https://docs.google.com/spreadsheets/d/1HNOo2qVf2xHo0hffNMKdxcBflXKFEVsQrTL-Uqadqas
  // copy the spreadsheet for other resistor in Voltage divider (thermistor/resistor)

// project/circuit components:
  // Digispark Attini85
  // MOSFET N-channel transistor (IRF520 G,D,S), needs heatsink on big output power (<500mA, 9A max)
  // resistors: 4.7 - 15 kOhm (depending on the calculations), 10kOhm
  // diode 4001 (when MOSFET loads with motors and not contain internal diode)
  // NTC thermistor 10kOhm
  // ventilator (12V)
  // powersource (12V)
  // powering choice:
    // a. voltage stabilizer 7805 (cooling not needed)
    // b. or use Attini onboard 5V stabilizer

// define output limits
// MINIMUM defines where ventilator begins rotation (not only squeezing).
#define MINIMUM 48
#define MAXIMUM 254

// COEF is used for fill the range in "byte" output values corresponds to limited input range
// from (P2) values from 600/2.99V/23°C to 800/3.99V/50°C
#define COEF 1.25

boolean light = true; // for blinking with internal LED

void setup() {
  pinMode(1, OUTPUT); // internal LED output
  pinMode(4, OUTPUT); // PWM on 1007 Hz, connected to MOSFET gate
  pinMode(2, INPUT);  // (+) >> 10k NTC thermistor >> (P2) >> 15k resistor >> (GND)
}

void loop() {
  // analogRead(1) from P2 >> 0-5V input >> 0 - 1023 value
  byte val = min( MAXIMUM, max( MINIMUM, analogRead(1) - 600 ) * COEF );

  analogWrite(4, val);
  digitalWrite(1, ( light ? HIGH : LOW )); // set internal LED state
  light = !light;  // switch LED
  unsigned int del = 1300 - (5*val); // long delay on lower temp (slowly blinking/looping)
  delay ( del );
}
