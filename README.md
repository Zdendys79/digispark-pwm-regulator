# Digispark PWM regulator

Arduino program for Digispark (Attiny85):
PWM regulator controlled by NTC thermistor.

* [Spreasheet for computing values](https://docs.google.com/spreadsheets/d/1HNOo2qVf2xHo0hffNMKdxcBflXKFEVsQrTL-Uqadqas)

* [Circuit diagram](https://www.circuit-diagram.org/editor/c/f71e125f9115474baeb99b27f20d0082)